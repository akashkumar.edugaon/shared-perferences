package com.example.shared_preferences

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class LoginModels (
    @Expose
    @SerializedName("user/login?_format=json")
    val entryToken:List<LoginModal>
)
data class LoginModal(
    @Expose
    @SerializedName("name")
    val login_name: String,

    @Expose
    @SerializedName("email")
    val login_pass: String

)