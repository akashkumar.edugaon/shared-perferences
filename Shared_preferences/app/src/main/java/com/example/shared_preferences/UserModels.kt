package com.example.shared_preferences

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UserModels (
    @SerializedName("rest/session/token")
    val entryToken:String? = null
)