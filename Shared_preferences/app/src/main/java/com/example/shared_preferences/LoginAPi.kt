package com.example.shared_preferences

import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Headers

interface LoginAPi {
            @Headers("X-CSRF-Token: R_3xW8JqmAV75u6sTiN48Ajm4m2DZj40kTiy5WN3DyY")
    @GET("user/login?_format=json")
    fun getLogin(): Observable<LoginModels>
    companion object Factory{

        fun create():UserApi{
            val retrofit= Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://edugaonlabs.com/")
                .build()
            return retrofit.create(UserApi::class.java)

        }

    }
}