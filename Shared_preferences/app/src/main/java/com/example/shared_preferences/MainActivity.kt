package com.example.shared_preferences

import android.media.MediaMetadataRetriever
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.ContactsContract
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MainActivity : AppCompatActivity() {

    lateinit var edtName:EditText
    lateinit var edtEmail:EditText
    lateinit var btnSave:Button
    lateinit var btnRetriever:Button
    lateinit var btnClear:Button
    lateinit var showname:TextView
    lateinit var showemail:TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val sharedPreference:SharedPreference=SharedPreference(this)

        edtName=findViewById(R.id.edt_name)
        edtEmail=findViewById(R.id.edt_email)
        btnClear=findViewById(R.id.btn_clear)
        btnSave=findViewById(R.id.btn_save)
        btnRetriever=findViewById(R.id.btn_retriev)
        showname=findViewById(R.id.show_name)
        showemail=findViewById(R.id.show_email)


        btnSave.setOnClickListener {

            val name=edtName.editableText.toString()
            val email=edtEmail.editableText.toString()

            sharedPreference.save("name",name)
            sharedPreference.save("email",email)
            Toast.makeText(this@MainActivity,"Data Stored",Toast.LENGTH_SHORT).show()

        }
        btnRetriever.setOnClickListener {
            if (sharedPreference.getValueString("name")!=null) {
                showname.text = sharedPreference.getValueString("name")!!
                Toast.makeText(this@MainActivity,"Data Retrieved",Toast.LENGTH_SHORT).show()
            }else{
                showname.text="NO value found"
            }
            if (sharedPreference.getValueString("email")!=null) {
                showemail.text = sharedPreference.getValueString("email")!!
            }else{
                showemail.text="No value found"
            }


        }

        btnClear.setOnClickListener {
            sharedPreference.clearSharedPreference()
            Toast.makeText(this@MainActivity,"Data Cleared",Toast.LENGTH_SHORT).show()
        }
    loadApi()
    }

    fun loadApi(){
        val ApiServce=LoginAPi.create()

        ApiServce.getLogin()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({result->
                result.entryToken
            },{error->
                error.printStackTrace()
            })
    }
}
