package com.example.shared_preferences

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        loadApi()
    }

    fun loadApi(){
        val ApiServce=LoginAPi.create()

        ApiServce.getLogin()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe({result->
                result.entryToken
            },{error->
                error.printStackTrace()
            })
    }
    private fun renderHistory(LoginList: String?){

    }
}